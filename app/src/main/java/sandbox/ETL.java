package sandbox;
import java.util.Map;
import java.util.List;


/**
 * Processor class to parse a specific type of log
 * composed by a header and a data section.
 *
 * Header with values separated by tabular
 *   * Timestamp
 *   * TAG
 *   * [MESSAGE TYPE WITHING BRACKETS]
 *   * Data
 *
 * Data Section starts right away after 'data:' and consists on
 * tuples of 4 elements
 *
 * - Event ID
 * - Call ID
 * - Access Code
 * - URL
 *
 * Data sample:
 *
 * 2021-07-06T17:26:41+00:00\tTAG\t[MESSAGE TYPE]\tdata: 1-5-1-www.google.com/url?key=value 0-312-0.23-http://www.cnn.com/news=123123
 */
class ETL {


    /**
     * Return the header as map. Using tab as separator and report each field into a
     * specific Key.
     *
     * For example:
     * timestamp: "2021-07-06T17:26:41+00:00"
     * server: "MainHost"
     * messageType: "messaged" // Note that brackets have beed deleted message:
     * data: 1-5-12-www.google.....
     **/
    public Map<String, String> parseHeader(String data) {


        return null;
    }

    /**
     * Returns all elements for the data message Where each data set has the next
     * format: Event ID - Call ID - Access Code - URL
     *
     * For example, for 1-5-12-www.google.com the parsed values will be: eventID = 1
     * callID = 5 accessCode = 12 url = www.google.com
     *
     * This function has to return a list of all these elements being parsed on a
     * Map Container
     **/
    public List<Map<String, String>> parseData(String data) {

        return null;
    }

    /**
    * TestCase for parseHeader
    */
  
    public static Boolean testParseHeader(){

        final String message = "2021-07-06T17:26:41+00:00\tMainHost\t[messaged]\tdata: 1-5-12-www.google.com 0-312-0.23-http://www.cnn.com/news=123123   1-None-None-htttp://cisco-businness.com";

        ETL app = new ETL();
        Map<String, String> outputMap = app.parseHeader(message);

        // Container shouldn't be empty
        if (outputMap.isEmpty())
            return false;

        // Checking right timestamp
        if (outputMap.containsKey("timestamp")){
            if (!outputMap.get("timestamp").equals("2021-07-06T17:26:41+00:00")) {
                return false;
            }
        }

        // Checking message type
        if (outputMap.containsKey("messageType") &&
           !outputMap.get("messageType").equals("messaged"))
            return false;

        // Checking for data
        if (outputMap.containsKey("data") && 
           !outputMap.get("data").equals("1-5-12-www.google.com 0-312-0.23-http://www.cnn.com/news=123123   1-None-None-htttp://cisco-businness.com"))
            return false;
        
        return true;
    }

    /**
    * Test Case for parseData
    */
    public static Boolean testParseData() {

        final String message = "2021-07-06T17:26:41+00:00\tMainHost\t[messaged]\tdata: 1-5-12-www.google.com 0-312-0.23-http://www.cnn.com/news=123123   1-None-None-htttp://cisco-businness.com";

        ETL app = new ETL();
        List<Map<String, String>> dataList = app.parseData(message);

        // Data list should have 3 elements
        if (dataList.size() !=  3)
            return false;

        // each element should have 4 items
        Map<String, String> data1 = dataList.get(0);
        if (data1.size() != 4)
            return false;

        if (!data1.containsKey("eventID")
                || !data1.containsKey("callID")
                || !data1.containsKey("accessCode")
                || !data1.containsKey("url"))
            return false;

        // Checking second element
        Map<String, String> data2 = dataList.get(1);
        // Checks for EventID, CallID, accessCOde and URL
        String accessCode2 = data2.get("accessCode");
        if (!data1.equals(0.23)) {
            return false;
        }

        String url2 = data2.get("url");
        if (!url2.equals("http://www.cnn.com/news=123123")){
            return false;
        }
        // Checking for third element
        Map<String, String> data3 = dataList.get(2);
        String url3 = data3.get("url");
        if (!url3.equals("htttp://cisco-businness.com")){
            return false;
        }


        return true;
    }
  


}
